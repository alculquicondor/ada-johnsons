#ifndef GRAPH_H
#define GRAPH_H

#include "common.h"

class Graph
{
    friend class Distance;
    friend class BellmanFord;
    friend class Dijkstra;
    struct edge {
        size_t v;
        itype cost;
        edge() {}
        edge (size_t v, itype c) : v(v), cost(c) {}
    };

public:
    Graph();
    Graph(size_t n);
    Graph(const Graph &graph);
    inline size_t n() const {
        return adjacency_.size();
    }
    inline void resize(size_t n) {
        adjacency_.resize(n);
    }
    const vector<edge> &adjacency(size_t u) const {
        return adjacency_[u];
    }
    void addEdge(size_t u, size_t v, itype c);
private:
    vector<vector<edge>> adjacency_;
    vector<edge> &adjacency(size_t u) {
        return adjacency_[u];
    }
};

#endif // GRAPH_H
