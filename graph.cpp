#include "graph.h"

Graph::Graph()
{
}

Graph::Graph(size_t n) : adjacency_(n)
{
}

Graph::Graph(const Graph &graph) : adjacency_(graph.adjacency_)
{
}

void Graph::addEdge(size_t u, size_t v, itype c) {
    adjacency_[u].push_back(edge(v, c));
}
