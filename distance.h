#ifndef DISTANCE_H
#define DISTANCE_H

#include "graph.h"
#include "bellmanford.h"
#include "dijkstra.h"

class Distance
{
public:
    Distance(const Graph *graph);
    itype operator()(size_t u, size_t v) const;
    void init();

private:
    const Graph *graph;
    vector<vector<itype>> distance;
};

#endif // DISTANCE_H
