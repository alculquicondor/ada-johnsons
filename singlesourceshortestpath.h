#ifndef SINGLESOURCESHORTESTPATH_H
#define SINGLESOURCESHORTESTPATH_H

#include "graph.h"

class SingleSourceShortestPath
{
public:
    SingleSourceShortestPath(const Graph *graph);
    virtual void init(size_t src) = 0;
    inline itype operator()(size_t u) {
        return distance[u];
    }

protected:
    const Graph *graph;
    std::vector<itype> distance;
};

#endif // SINGLESOURCESHORTESTPATH_H
