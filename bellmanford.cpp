#include "bellmanford.h"

BellmanFord::BellmanFord(const Graph *graph) : SingleSourceShortestPath(graph),
    inqueue(graph->n())
{
}

void BellmanFord::init(size_t src)
{
    distance.assign(graph->n(), infinity);
    distance[src] = 0;
    queue<size_t> Q;
    Q.push(src);
    inqueue[src] = true;
    while (not Q.empty()) {
        size_t u = Q.front();
        Q.pop();
        inqueue[u] = false;
        for (const Graph::edge &e : graph->adjacency_[u]) {
            itype cost = distance[u] + e.cost;
            if (distance[e.v] > cost) {
                distance[e.v] = cost;
                if (not inqueue[e.v]) {
                    Q.push(e.v);
                    inqueue[e.v] = true;
                }
            }
        }
    }
}
