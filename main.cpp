#include <iostream>
#include <cassert>
#include <chrono>

#include "distance.h"

using namespace std;

#ifndef QT_NO_DEBUG
void test()
{
    MinHeap mheap(5);
    assert(mheap.top() == 0);
    mheap.updatePriority(4, 4);
    assert(mheap.top() == 4);
    mheap.updatePriority(2, 3);
    assert(mheap.top() == 2);
    mheap.updatePriority(3, 1);
    assert(mheap.top() == 3);
    mheap.updatePriority(0, 2);
    assert(mheap.top() == 3);
    mheap.updatePriority(1, 0);
    assert(mheap.top() == 1);
    mheap.pop();
    assert(mheap.top() == 3);
    mheap.pop();
    assert(mheap.top() == 0);
    mheap.pop();
    assert(mheap.top() == 2);
    mheap.pop();
    assert(mheap.top() == 4);

    Graph graph(5);
    graph.addEdge(0, 1, 5);
    graph.addEdge(1, 2, 3);
    graph.addEdge(1, 3, 5);
    graph.addEdge(3, 0, 4);
    graph.addEdge(3, 2, 15);
    graph.addEdge(2, 4, 1);
    graph.addEdge(4, 3, 2);
    BellmanFord bellmanFord(&graph);
    bellmanFord.init(0);
    assert(bellmanFord(1) == 5);
    assert(bellmanFord(2) == 8);
    assert(bellmanFord(3) == 10);
    assert(bellmanFord(4) == 9);
    Dijkstra dijkstra(&graph);
    dijkstra.init(0);
    assert(dijkstra(1) == 5);
    assert(dijkstra(2) == 8);
    assert(dijkstra(3) == 10);
    assert(dijkstra(4) == 9);
    Distance distance(&graph);
    distance.init();
    assert(distance(0, 1) == 5);
    assert(distance(1, 2) == 3);
    assert(distance(2, 3) == 3);
    assert(distance(3, 2) == 12);
    assert(distance(0, 2) == 8);
    assert(distance(0, 3) == 10);
    assert(distance(1, 0) == 9);
    assert(distance(4, 1) == 11);
    graph.resize(6);
    graph.addEdge(0, 5, 1);
    graph.addEdge(5, 3, 1);
    graph.addEdge(3, 1, 2);
    Distance distance2(&graph);
    distance2.init();
    assert(distance2(0, 2) == 7);
    assert(distance2(0, 3) == 2);
    assert(distance2(3, 1) == 2);
    assert(distance2(3, 2) == 5);
    assert(distance2(2, 3) == 3);
    assert(distance2(1, 5) == 10);
    cout << "Tests Finished Successfully!" << endl;
}
#endif

void min_dist(const Distance &distance, size_t n)
{
    cout << "Minimum Distance: ";
    itype ans = infinity;
    for (size_t u = 0; u < n; ++u)
        for (size_t v = 0; v < n; ++v)
            ans = min(ans, distance(u, v));
    cout << ans << endl;
}

void max_dist(const Distance &distance, size_t n)
{
    cout << "Maximum Distance: ";
    itype ans = 0;
    for (size_t u = 0; u < n; ++u)
        for (size_t v = 0; v < n; ++v)
            ans = max(ans, distance(u, v));
    cout << ans << endl;
}

int main()
{
#ifndef QT_NO_DEBUG
    test();
#endif
    ios::sync_with_stdio(0);

    size_t n, m;
    cin >> n >> m;
    size_t u, v;
    itype c;
    Graph graph(n);
    for (size_t i = 0; i < m; ++i) {
        cin >> u >> v >> c;
        graph.addEdge(--u, --v, c);
    }
    Distance distance(&graph);

    chrono::time_point<chrono::system_clock> startTime, endTime;
    startTime = chrono::system_clock::now();
    distance.init();
    endTime = chrono::system_clock::now();
    int duration = chrono::duration_cast<chrono::milliseconds>(endTime-startTime).count();
    cout << "Calculated in " << duration << "ms" << endl;

    min_dist(distance, n);
    max_dist(distance, n);

    return 0;
}

