#include "minheap.h"

MinHeap::MinHeap(size_t n) : heap(n), position(n) {
    for (size_t i = 0; i < n; ++i) {
        heap[i] = entry(i, infinity);
        position[i] = i;
    }
}

void MinHeap::pop()
{
    position[heap.back().value] = 0;
    heap[0] = heap.back();
    heap.resize(heap.size()-1);
    heapifyDown(0);
}

void MinHeap::updatePriority(std::size_t id, itype priority)
{
    size_t p = position[id];
    bool less = priority < heap[p].priority;
    heap[p].priority = priority;
    if (less)
        heapifyUp(p);
    else
        heapifyDown(p);
}

void MinHeap::heapifyUp(std::size_t pos)
{
    size_t par;
    while (pos) {
        par = parent(pos);
        if (heap[par].priority > heap[pos].priority) {
            swap(position[heap[par].value], position[heap[pos].value]);
            swap(heap[par], heap[pos]);
            pos = par;
        } else {
            break;
        }
    }
}

void MinHeap::heapifyDown(std::size_t pos)
{
    while (left(pos) < heap.size()) {
        size_t npos = pos, l = left(pos), r = right(pos);
        if (heap[l].priority < heap[npos].priority)
            npos = l;
        if (r < heap.size() and heap[r].priority < heap[npos].priority)
            npos = r;
        if (npos != pos) {
            swap(position[heap[pos].value], position[heap[npos].value]);
            swap(heap[pos], heap[npos]);
            pos = npos;
        } else {
            break;
        }
    }
}
