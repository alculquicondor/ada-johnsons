TEMPLATE = app
CONFIG += console
CONFIG += c++11
CONFIG -= app_bundle
CONFIG -= qt

CONFIG(release, debug|release) {
  DEFINES += QT_NO_DEBUG
}

SOURCES += main.cpp \
    graph.cpp \
    distance.cpp \
    bellmanford.cpp \
    dijkstra.cpp \
    singlesourceshortestpath.cpp \
    minheap.cpp

HEADERS += \
    graph.h \
    distance.h \
    bellmanford.h \
    dijkstra.h \
    singlesourceshortestpath.h \
    minheap.h \
    common.h

