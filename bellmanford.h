#ifndef BELLMANFORD_H
#define BELLMANFORD_H

#include "singlesourceshortestpath.h"
#include <queue>

using std::queue;

class BellmanFord : public SingleSourceShortestPath
{
public:
    BellmanFord(const Graph *graph);
    void init(size_t src);
    inline itype operator()(size_t u) {
        return distance[u];
    }

private:
    vector<bool> inqueue;
};

#endif // BELLMANFORD_H
