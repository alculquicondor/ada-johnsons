#ifndef DIJKSTRA_H
#define DIJKSTRA_H

#include "singlesourceshortestpath.h"
#include "minheap.h"

class Dijkstra : public SingleSourceShortestPath
{
public:
    Dijkstra(const Graph *graph);
    void init(size_t src);
private:
    vector<bool> visited;
};

#endif // DIJKSTRA_H
