#include "dijkstra.h"

Dijkstra::Dijkstra(const Graph *graph) : SingleSourceShortestPath(graph),
    visited(graph->n())
{
}

void Dijkstra::init(size_t src)
{
    distance.assign(graph->n(), infinity);
    visited.assign(graph->n(), false);
    distance[src] = 0;
    MinHeap Q(graph->n());
    Q.updatePriority(src, 0);
    while (not Q.empty()) {
        size_t u = Q.top();
        Q.pop();
        if (visited[u])
            continue;
        visited[u] = true;
        for (const Graph::edge &e : graph->adjacency_[u]) {
            itype t = distance[u] + e.cost;
            if (distance[e.v] > t) {
                distance[e.v] = t;
                Q.updatePriority(e.v, t);
            }
        }
    }
}
