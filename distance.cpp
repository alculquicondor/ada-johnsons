#include "distance.h"

Distance::Distance(const Graph *graph) : graph(graph),
    distance(graph->n(), vector<itype>(graph->n(), infinity))
{
}

itype Distance::operator()(size_t u, size_t v) const
{
    return distance[u][v];
}

void Distance::init()
{
    Graph g2(*graph);
    g2.resize(graph->n() + 1);
    for (size_t u = 0; u < graph->n(); ++u)
        g2.addEdge(graph->n(), u, 0);
    BellmanFord bellmanFord(&g2);
    bellmanFord.init(graph->n());
    for (size_t u = 0; u < graph->n(); ++u) {
        for (Graph::edge &e : g2.adjacency(u))
            e.cost += bellmanFord(u) - bellmanFord(e.v);
    }
    g2.resize(graph->n());
    Dijkstra dijkstra(&g2);
    for (size_t u = 0; u < graph->n(); ++u) {
        dijkstra.init(u);
        for (size_t v = 0; v < graph->n(); ++v)
            distance[u][v] = dijkstra(v) - bellmanFord(u) + bellmanFord(v);
    }
}
