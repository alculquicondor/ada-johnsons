#ifndef MINHEAP_H
#define MINHEAP_H

#include "common.h"

using std::swap;

class MinHeap
{
    struct entry {
        size_t value;
        itype priority;
        entry() {}
        entry(size_t v, itype p) : value(v), priority(p) {}
    };

public:
    MinHeap(size_t n);
    inline size_t top() const {
        return heap.front().value;
    }
    bool empty() {
        return heap.empty();
    }
    void pop();
    void updatePriority(size_t id, itype priority);
private:
    vector<entry> heap;
    vector<size_t> position;
    void heapifyUp(size_t pos);
    void heapifyDown(size_t pos);
    inline size_t parent(size_t p) {
        return (p-1) >> 1;
    }
    inline size_t left(size_t p) {
        return (p << 1) + 1;
    }
    inline size_t right(size_t p) {
        return (p << 1) + 2;
    }
};


#endif // MINHEAP_H
